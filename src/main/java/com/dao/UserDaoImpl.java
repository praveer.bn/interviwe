package com.dao;

import com.connection.JpaProvider;
import com.entity.User;

import javax.persistence.EntityManager;

public class UserDaoImpl implements UserDao{
    public void insertUser(User user) {
        EntityManager entityManager= JpaProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
