package com.dao;

import com.entity.Vehicle;

import java.util.List;

public interface VehicleDao {
    List<Vehicle> showVehicle();
}
