package com.dao;

import com.connection.JpaProvider;
import com.entity.Vehicle;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class VehicleDaoImpl implements VehicleDao{
    public List<Vehicle> showVehicle() {
        EntityManager entityManager= JpaProvider.getEntityManagerFactory().createEntityManager();
        Query query=entityManager.createQuery("from Vehicle");
        List<Vehicle> list=query.getResultList();
        return list;
    }
}
