package com.entity;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
@Entity
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int bookingId;
    private Date date;
    private Time startTime;
    private Time endTime;
    private float totalFare;
    @ManyToOne
    private User user;
    @ManyToOne
    private Vehicle vehicle;

    public Booking() {
    }

    public Booking(Date date, Time startTime, Time endTime, float totalFare, User user, Vehicle vehicle) {
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.totalFare = totalFare;
        this.user = user;
        this.vehicle = vehicle;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    public float getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(float totalFare) {
        this.totalFare = totalFare;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "bookingId=" + bookingId +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", totalFare=" + totalFare +
                ", user=" + user +
                ", vehicle=" + vehicle +
                '}';
    }
}
