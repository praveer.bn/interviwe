package com.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int vehicleId;
    private String modelName;
    private String registerNumber;
    private String vehicleType;
    private int vehicleOccupancy;
    private int farePerHour;
    @OneToMany(mappedBy = "vehicle",cascade = CascadeType.ALL)
    private List<Booking> bookingList;

    public Vehicle() {
    }

    public Vehicle(String modelName, String registerNumber, String vehicleType, int vehicleOccupancy, int farePerHour) {
        this.modelName = modelName;
        this.registerNumber = registerNumber;
        this.vehicleType = vehicleType;
        this.vehicleOccupancy = vehicleOccupancy;
        this.farePerHour = farePerHour;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(String registerNumber) {
        this.registerNumber = registerNumber;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public int getVehicleOccupancy() {
        return vehicleOccupancy;
    }

    public void setVehicleOccupancy(int vehicleOccupancy) {
        this.vehicleOccupancy = vehicleOccupancy;
    }

    public int getFarePerHour() {
        return farePerHour;
    }

    public void setFarePerHour(int farePerHour) {
        this.farePerHour = farePerHour;
    }

    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "vehicleId=" + vehicleId +
                ", modelName='" + modelName + '\'' +
                ", registerNumber='" + registerNumber + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", vehicleOccupancy=" + vehicleOccupancy +
                ", farePerHour=" + farePerHour +
                ", bookingList=" + bookingList +
                '}';
    }
}
