package com.servlets;

import com.dao.VehicleDaoImpl;
import com.entity.Vehicle;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/booking")
public class BookingVehicle extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        ServletContext servletContext=req.getServletContext();
        int uid= Integer.parseInt(servletContext.getAttribute("uid").toString());
        servletContext.setAttribute("uid",uid);
        String vehicleType=req.getParameter("vehicleType");
        int vehicleOccupancy= Integer.parseInt(req.getParameter("vehicleOccupancy"));
            VehicleDaoImpl vehicleDao=new VehicleDaoImpl();
            List<Vehicle> list= vehicleDao.showVehicle();
            List<Vehicle> filterlist =list.stream().filter(s->s.getVehicleOccupancy()==vehicleOccupancy && s.getVehicleType().equals(vehicleType)).collect(Collectors.toList());
            printWriter.print(" <form action=\"confirmbooking\">");
            printWriter.print("<table border=2px>");
            printWriter.print("<th>Vehicle id</th><th>model name</th><th>reg number</th><th>vehicle type</th><th>vehicle occupancy</th><th>Fare Per hour</th>");
            for (Vehicle s : filterlist) {
                printWriter.println("<tr><td>" + s.getVehicleId() + "</td><td>" + s.getModelName() + "</td><td>" + s.getRegisterNumber() + "</td><td>" + s.getVehicleType() + "</td><td>" + s.getVehicleOccupancy() + "</td><td>" + s.getFarePerHour() + "</td><td><input type=\"radio\" name=\"option\" value=" + s.getVehicleId() + ">book</th></td></tr>");
            }
            printWriter.print(" <input type=\"date\" name=\"date\" required><br>\n" +
                    "       <input type=\"time\" name=\"startTime\" required><br>\n" +
                    "       <input type=\"time\" name=\"endTime\" required>");
            printWriter.print(" <tr>\n" +
                    "                    <th><input type=\"submit\"></th>\n" +
                    "                </tr>");
            printWriter.print("</form>");


    }
}
