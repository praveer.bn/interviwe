package com.servlets;

import com.connection.JpaProvider;
import com.entity.Booking;
import com.entity.User;
import com.entity.Vehicle;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;
import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/confirmbooking")
public class ConfirmBooking extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        ServletContext servletContext=req.getServletContext();
        Time startTime= Time.valueOf(req.getParameter("startTime")+":00");
        Time endTime= Time.valueOf(req.getParameter("endTime")+":00");
        Date date=Date.valueOf(req.getParameter("date"));
        int vid= Integer.parseInt(req.getParameter("option"));
        int uid= Integer.parseInt(servletContext.getAttribute("uid").toString());
        EntityManager entityManager= JpaProvider.getEntityManagerFactory().createEntityManager();
        Query query= entityManager.createQuery("from Booking");
        Duration duration = Duration.between(startTime.toLocalTime(), endTime.toLocalTime());

        List<Booking> bookingList=query.getResultList();
        List<Booking> bookingVehicleList=bookingList.stream().filter(s->s.getDate()==date).collect(Collectors.toList());
        if(bookingVehicleList.size()!=0) {
            List<Booking> bookingList1=bookingVehicleList.stream().filter(s->startTime.after(s.getEndTime())&&  endTime.before(s.getStartTime())).collect(Collectors.toList());
            if (bookingList1.size()==0) {
                if (duration.isNegative()) {
                    RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                    printWriter.print("plz check the time");
                    requestDispatcher.include(req, resp);
                } else {
                    Vehicle vehicle = entityManager.find(Vehicle.class, vid);
                    User user = entityManager.find(User.class, uid);
                    float TotalFare = duration.toHours() * vehicle.getFarePerHour();
                    Booking booking = new Booking(date, startTime, endTime, TotalFare, user, vehicle);
                    RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                    entityManager.getTransaction().begin();
                    entityManager.persist(booking);
                    entityManager.getTransaction().commit();
                    entityManager.close();
                    printWriter.print("booking done");
                    requestDispatcher.include(req, resp);

                }
            }else {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                printWriter.print("on this time vehicle not available");
                requestDispatcher.include(req, resp);
            }
        }else{
            if (duration.isNegative()) {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                printWriter.print("plz check the time");
                requestDispatcher.include(req, resp);
            } else {
                Vehicle vehicle = entityManager.find(Vehicle.class, vid);
                User user = entityManager.find(User.class, uid);
                float TotalFare = duration.toHours() * vehicle.getFarePerHour();
                Booking booking = new Booking(date, startTime, endTime, TotalFare, user, vehicle);
                entityManager.getTransaction().begin();
                entityManager.persist(booking);
                entityManager.getTransaction().commit();
                entityManager.close();
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                printWriter.print("booking done");
                requestDispatcher.include(req, resp);

            }
        }

    }
}
