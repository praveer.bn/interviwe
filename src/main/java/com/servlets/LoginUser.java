package com.servlets;

import com.connection.JpaProvider;
import com.dao.UserDaoImpl;
import com.dao.VehicleDaoImpl;
import com.entity.User;
import com.entity.Vehicle;

import javax.persistence.EntityManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/loginUser")
public class LoginUser extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        String name=req.getParameter("uName").toLowerCase().trim();
        int password= Integer.parseInt(req.getParameter("password"));
        int uid= Integer.parseInt(req.getParameter("uId"));
        ServletContext servletContext=req.getServletContext();
        servletContext.setAttribute("uid",uid);
        EntityManager entityManager= JpaProvider.getEntityManagerFactory().createEntityManager();
        User user=entityManager.find(User.class,uid);
        if(uid==user.getUserId() && name.equals(user.getUserName()) && password==user.getUserPassword()){
            VehicleDaoImpl vehicleDao=new VehicleDaoImpl();
           List<Vehicle> list= vehicleDao.showVehicle();
            List<Vehicle> sortedlist =list.stream().sorted(Comparator.comparing(Vehicle::getFarePerHour)).collect(Collectors.toList());
            printWriter.print(" <form action=\"booking\">");
            printWriter.print("<table border=2px>");
            printWriter.print("<th>Vehicle id</th><th>model name</th><th>reg number</th><th>vehicle type</th><th>vehicle occupancy</th><th>Fare Per hour</th>");

            for (Vehicle s : sortedlist) {
                printWriter.println("<tr><td>" + s.getVehicleId()+ "</td><td>" + s.getModelName() + "</td><td>" + s.getRegisterNumber()+ "</td><td>" + s.getVehicleType() + "</td><td>" + s.getVehicleOccupancy() + "</td><td>" + s.getFarePerHour()+ "</td></tr>");
            }
            printWriter.print(" <select name=\"vehicleType\" >\n" +
                    "        <option value=\"2wheeler\">2wheeler</option>\n" +
                    "        <option value=\"4wheeler\">4wheeler</option>\n" +
                    "       </select>\n" +
                    "       <select name=\"vehicleOccupancy\" >\n" +
                    "        <option value=\"2\">2</option>\n" +
                    "        <option value=\"4\">4</option>\n" +
                    "        <option value=\"6\">6</option>\n" +
                    "       </select>");
            printWriter.print(" <tr>\n" +
                    "                    <th><input type=\"submit\"></th>\n" +
                    "                </tr>");
            printWriter.print("</form>");

        }else{
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("index.jsp");
            printWriter.print("invalid User name or password");
            requestDispatcher.include(req,resp);
        }

    }
}
