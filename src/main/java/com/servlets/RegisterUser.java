package com.servlets;

import com.dao.UserDaoImpl;
import com.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/registerUser")
public class RegisterUser extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        String name=req.getParameter("uName").toLowerCase().trim();
        int password= Integer.parseInt(req.getParameter("password"));
        User user=new User(name,password);
        UserDaoImpl userDao=new UserDaoImpl();
        userDao.insertUser(user);
        RequestDispatcher requestDispatcher= req.getRequestDispatcher("index.jsp");
        printWriter.print("User Added");
        requestDispatcher.include(req,resp);
    }
}
